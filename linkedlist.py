import functools


def check_types(method):
    @functools.wraps(method)
    def wrapper(self, val, *args, **kwargs):
        if not isinstance(val, self.type_of_in):
            raise TypeError
        return method(self, val, *args, **kwargs)

    return wrapper


class Node:
    def __init__(self, val, next_element: 'Node' = None):
        self.val = val
        self.next = next_element


class LinkedList:
    def __init__(self, type_of_in=int):
        self.head = None
        self.type_of_in = type_of_in

    def __str__(self):
        if self.head is not None:
            current = self.head
            out = str(current.val) + ' '
            while current.next is not None:
                current = current.next
                out += str(current.val) + ' '
            return out

    def clear(self):
        self.__init__()

    def __len__(self):
        length = 0
        if self.head is not None:
            current = self.head
            while current.next is not None:
                current = current.next
                length += 1
        return length + 1

    @check_types
    def add_begin(self, val):
        new_head = Node(val, self.head)
        self.head = new_head

    def del_item(self, i):
        if self.head is None:
            return
        current = self.head
        count = 0
        prev = 0
        if i == 0:
            self.head = self.head.next
            return
        while current is not None:
            if count == i:
                if current.next is None:
                    self.head = current
                prev.next = current.next
                break
            prev = current
            current = current.next
            count += 1

    def check_emty_list(self):
        return len(self) == 0

    @check_types
    def insert_nth(self, x, i):
        if self.head is None:
            self.head = Node(x, None)
            return
        if type(i) != int:
            raise TypeError
        if i == 0:
            self.head = Node(x, self.head)
            return
        curr = self.head
        count = 0
        while curr is not None:
            count += 1
            if count == i:
                curr.next = Node(x, curr.next)
                break
            curr = curr.next

    def __getitem__(self, i):
        length = 0
        current = None
        if self.head is not None:
            current = self.head
            while i != length:
                current = current.next
                length += 1
            if i == length:
                current = current.val
        return current


class Iterator:
    def __init__(self, data):
        self.data = data
        self.index = -1
        self.index_last = len(data)

    def __iter__(self):
        return self

    def __next__(self):
        if self.index == self.index_last - 1:
            raise StopIteration
        self.index = self.index + 1
        return self.data[self.index]


if __name__ == '__main__':
    l = LinkedList()
    l.add_begin(3)
    print(l)
    print(l.del_item(3))
    print(l.check_emty_list())
    print(l.insert_nth(1, 2))
    print(l)
    for i in Iterator(l):
        print(i)
    print(l)
    print(l.clear())

